import graphene

from books.schema import Query as BooksQuery
from books.schema import Mutation as BooksMutation
from authors.schema import Query as AuthorsQuery
from authors.schema import Mutation as AuthorsMutation


class Query(BooksQuery, AuthorsQuery, graphene.ObjectType):
    # This class will inherit from multiple Queries
    # as we begin to add more apps to our project
    pass
  
class Mutation(BooksMutation, AuthorsMutation, graphene.ObjectType):
    pass

schema = graphene.Schema(query=Query,mutation=Mutation)