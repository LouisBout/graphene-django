from rest_framework import serializers
from books.serializers import BookSerializer
from .models import Author

class AuthorSerializer(serializers.ModelSerializer):
  books = BookSerializer(many=True, read_only=True)
  class Meta:
    model = Author
    fields = ('first_name', 'last_name', 'books', 'friends')