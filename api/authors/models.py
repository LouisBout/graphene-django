from django.db import models

# Create your models here.
class Author(models.Model):
  first_name = models.CharField(max_length=50)
  last_name = models.CharField(max_length=50)
  friends = models.ManyToManyField('self', blank=True)

  class Meta:
    verbose_name_plural = "Authors"