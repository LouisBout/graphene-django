from graphene import ObjectType

from graphene_django_extras import DjangoSerializerType
from graphene_django_extras.paginations import PageGraphqlPagination
from graphene_django.forms.mutation import DjangoModelFormMutation

from .serializers import AuthorSerializer

# class AuthorType(DjangoObjectType):
#     pk = Int(source='id')
#     class Meta:
#         model = Author
#         filter_fields = {
#             'first_name': ['exact', 'icontains', 'istartswith'],
#             'last_name': ['exact', 'icontains', 'istartswith']
#         }

class AuthorType(DjangoSerializerType):
    class Meta:
        serializer_class = AuthorSerializer
        pagination = PageGraphqlPagination(page_size=5,page_size_query_param="page_size") # ordering can be: string, tuple or list
        filter_fields = {
            'first_name': ['exact', 'icontains', 'istartswith'],
            'last_name': ['exact', 'icontains', 'istartswith']
        }


class Query(object):
    # author = DjangoObjectField(AuthorType, description= 'Single User query')
    # all_authors = DjangoFilterPaginateListField(AuthorType, pagination=LimitOffsetGraphqlPagination())
    # author = AuthorType.RetrieveField(description='Some description message for retrieve query')
    # all_authors = AuthorType.ListField(description='Some description message for list query')
    author, all_authors = AuthorType.QueryFields(description='Some description message for both queries')


# class CreateAuthorMutation(DjangoModelFormMutation):
#     class Meta:
#         form_class = AuthorForm


class Mutation(ObjectType):
    # author_create = CreateAuthorMutation.Field()
    author_create = AuthorType.CreateField(description='Description message for create')
    author_delete = AuthorType.DeleteField(description='Description message for delete')
    author_update = AuthorType.UpdateField(description='Description message for update')