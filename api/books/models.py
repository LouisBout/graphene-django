from django.db import models

from authors.models import Author

# Create your models here.
class Book(models.Model):
  title = models.CharField(max_length=50)
  author = models.ForeignKey(Author, on_delete=models.CASCADE, related_name="books")

  class Meta:
    verbose_name_plural = "Books"