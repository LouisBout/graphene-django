# Generated by Django 2.1.5 on 2019-01-13 23:30

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('books', '0003_auto_20190112_2132'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='book',
            options={'verbose_name_plural': 'Books'},
        ),
    ]
