from graphene import ObjectType

from graphene_django_extras import DjangoSerializerType
from graphene_django_extras.paginations import LimitOffsetGraphqlPagination
from graphene_django.forms.mutation import DjangoModelFormMutation

from .serializers import BookSerializer

from .models import Book
from .forms import BookForm

class BookType(DjangoSerializerType):
    class Meta:
        serializer_class = BookSerializer
        pagination = LimitOffsetGraphqlPagination(default_limit=25) # ordering can be: string, tuple or list
        # Allow for some more advanced filtering here
        filter_fields = {
            'title': ['exact', 'icontains', 'istartswith'],
            'author': ['exact'],
            'author__first_name': ['exact'],
            'author__last_name': ['exact']
        }

class CreateBookMutation(DjangoModelFormMutation):
    class Meta:
        form_class = BookForm

class Query(object):
    book = BookType.RetrieveField(description='Some description message for retrieve query')
    all_books = BookType.ListField(description='Some description message for list query')


class Mutation(ObjectType):
    book_create = BookType.CreateField(description='Description message for create')
    book_delete = BookType.DeleteField(description='Description message for delete')
    book_update = BookType.UpdateField(description='Description message for update')
